package cz.vse.java.xvalm00.adventuracv.main;

public interface Observer {

    /**
     * reakce na pozorovanou změnu
     */
    void update();
}
