package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class HerniPlocha {

    private final HerniPlan plan;
    private final AnchorPane anchorPane = new AnchorPane();
    private Circle aktualniPozice;

    public HerniPlocha(HerniPlan plan){
        this.plan = plan;
        nastavHerniPlochu();
    }

    public AnchorPane getAnchorPane(){
        return anchorPane;
    }

    private void nastavHerniPlochu(){
        ImageView herniPlanImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream
                ("/zdroje/herniPlan.png"), 400, 250, false, false));

        aktualniPozice = new Circle(10, Paint.valueOf("red"));

        AnchorPane.setTopAnchor(aktualniPozice, plan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(aktualniPozice, plan.getAktualniProstor().getPosLeft());

        anchorPane.getChildren().addAll(herniPlanImageView, aktualniPozice);
    }

}
