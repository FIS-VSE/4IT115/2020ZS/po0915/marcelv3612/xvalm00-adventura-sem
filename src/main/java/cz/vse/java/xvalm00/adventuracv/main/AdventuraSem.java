package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.Hra;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import static cz.vse.java.xvalm00.adventuracv.logika.HerniPlan.Prostory.*;

public class AdventuraSem extends Application {

    private Button tlacitko;
    private PopiskyDlePoctuZmacknuti kolikratTlacitkoZmacknuto = PopiskyDlePoctuZmacknuti.NULA_KRAT;
    private final TextArea herniKonzole = new TextArea();
    private final IHra hra = new Hra();
    private FlowPane spodniFlowPane;
    private TextField prikazTextField;
    private final HerniPlan herniPlan = hra.getHerniPlan();
    private final HerniPlocha herniPlocha = new HerniPlocha(herniPlan);
    private final PanelVychodu panelVychodu = new PanelVychodu(hra.getHerniPlan());

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else if ("-text".equals(args[0])) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
            System.exit(0);
        }
    }

    @Override
    public void start(Stage primaryStage) {

        pripravTlacitkoProUcitele();
        pripravPrubehAdventurouTextArea();

        pripravSpodniPanel();

        inicializujZadavaniPrikazu();

        // Pridej prvky na layout
        BorderPane pane = new BorderPane();
        pane.setCenter(herniKonzole);
        pane.setBottom(spodniFlowPane);
        pane.setTop(herniPlocha.getAnchorPane());
        pane.setRight(panelVychodu.getListView());

        // Pridej layout na scenu
        Scene scene = new Scene(pane, 600, 450);

        // Finalize and show the stage
        primaryStage.setScene(scene);
        primaryStage.setTitle("Grafická adventura");
        prikazTextField.requestFocus();
        primaryStage.show();

    }

    private void inicializujZadavaniPrikazu() {
        prikazTextField.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String prikaz = prikazTextField.getText();
                prikazTextField.setText("");

                if ("ucitel".equals(prikaz)) {
                    tlacitko.setDisable(false);
                    return;
                }

                herniKonzole.appendText("\n" + prikaz + "\n");

                String odpovedHry = hra.zpracujPrikaz(prikaz);
                herniKonzole.appendText("\n" + odpovedHry + "\n");

                if (hra.konecHry()) {
                    prikazTextField.setEditable(false);
                }
            }
        });
    }

    private void pripravSpodniPanel() {
        spodniFlowPane = new FlowPane();
        Label zadejPrikazLabel = new Label("Zadej prikaz");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        prikazTextField = new TextField();
        spodniFlowPane.setAlignment(Pos.CENTER);
        spodniFlowPane.getChildren().addAll (zadejPrikazLabel, prikazTextField, tlacitko );
    }

    private void pripravPrubehAdventurouTextArea() {
        herniKonzole.setText(hra.vratUvitani());
        herniKonzole.setEditable(false);
    }

    private void pripravTlacitkoProUcitele() {
        tlacitko = new Button();
        tlacitko.setText(PopiskyDlePoctuZmacknuti.NULA_KRAT.getPopisek());
        tlacitko.setOnAction(event -> zmacknutoTlacitko());
        tlacitko.setDisable(true);
    }

    public enum PopiskyDlePoctuZmacknuti {
        NULA_KRAT("Zmáčkni mě."), JEDNOU("Zmáčkli mě."), DVAKRAT("Mačkáš mě."),
        TRIKRAT("Už radši ne.");

        private final String popisek;

        PopiskyDlePoctuZmacknuti(String popisek) {
            this.popisek = popisek;
        }

        private String getPopisek() {
            return this.popisek;
        }
    }

    private void zmacknutoTlacitko() {

        HerniPlan.Prostory aktualniProstor = HerniPlan.Prostory.prostor(herniPlan.getAktualniProstor().getNazev());

        switch (aktualniProstor) {
            case DOMECEK:
                Prostor sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(LES.nazev);
                automatickyPruchod(sousedniProstor);
                break;
            case LES:
                sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(HLUBOKY_LES.nazev);
                automatickyPruchod(sousedniProstor);
                break;
            case HLUBOKY_LES:
                sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(CHALOUPKA.nazev);
                automatickyPruchod(sousedniProstor);
                break;
        }

    }

    private void automatickyPruchod(Prostor sousedniProstor) {
        String odpovedHry = hra.zpracujPrikaz("běž " + sousedniProstor.getNazev());
        tlacitko.setText(sousedniProstor.getNazev() + " - jdeme dal?");
        herniKonzole.appendText("\n\n" + odpovedHry + "\n");
    }
}
