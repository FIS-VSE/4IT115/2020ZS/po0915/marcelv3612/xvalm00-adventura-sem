package cz.vse.java.xvalm00.adventuracv.main;

public interface Subject {

    /**
     * Registrace pozorovatele změn
     * @param observer
     */
    void registerObserver(Observer observer);

    /**
     * Odebrání ze seznamu pozorovatelů
     * @param observer
     */
    void unregisterObserver(Observer observer);

    /**
     * Upozornění registrovaných pozorovatelů na změnu
     */
    void notifyObservers();
}
